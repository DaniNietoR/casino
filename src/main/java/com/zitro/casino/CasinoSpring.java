package com.zitro.casino;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zitro.casino.bean.ResponseBean;
import com.zitro.casino.domain.Game;
import com.zitro.casino.domain.GameConfiguration;
import com.zitro.casino.domain.json.GameAdapter;
import com.zitro.casino.utils.Request;

@SpringBootApplication
public class CasinoSpring implements CommandLineRunner {

	private static final String URL_PATH = "http://localhost";
	private static final String URL_PORT = "8080";
	
	public static void main(String[] args) {
		SpringApplication.run(CasinoSpring.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {				

		//THREAD DE GENERACIÓN DE JUGADORES Y APUESTAS
		new Thread(new RunnableNewPlayer()).start();
	}
	
	/**
	 * Clase encargada de la generación aleatoria de jugadores y de las jugadas de Estos.
	 * Se añaden 2 jugadores por segundo siempre y cuando haya slots disponibles.
	 * En el caso de que se haya llegado el límite de jugadores no se creará ninguna partida nueva hasta que se libere algún slot.
	 */
	public class RunnableNewPlayer implements Runnable {

		public void run() {
			try {
				while (true) {
					Thread.sleep(500);
					ResponseBean responseBean = addNewPlayer();
					
					//Immediatamente después de la creación del juego se crearía otro thread donde el jugador iría haciendo apuestas
					//hasta que se le agote el tiempo o se quede sin dinero
					if (responseBean.getGame() != null) {
						new Thread(new RunnablePlayGame(responseBean.getGame())).start();
					}
					else {
						System.out.println("[ERROR] " + responseBean.getError());
						Thread.sleep(5000);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Clase encargada de ejecutar jugadas para un juego.
	 * Se ejecuta una jugada por segundo mientras no se acabe el tiempo del jugador o su saldo
	 */
	public class RunnablePlayGame implements Runnable {

		Game game;
		public RunnablePlayGame(Game game) {
			this.game = game;
		}
		
		public void run() {
			boolean stopThread = false;
			try {
				while (!stopThread) {
					Thread.sleep(1000);
					ResponseBean responseBean = bet(game, getRandomBet(game));
					//El balance se ha actualizado en el objeto del servicio pero el que hemos recuperado
					//es un objeto distinto por lo que es necesario actualizarlo
					if (responseBean.getTransaction() != null) {
						this.game.getUser().setBalance(responseBean.getTransaction().getGame().getUser().getBalance());
					}
					stopThread = responseBean.isEndGame();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Se llama al servicio para añadir una nueva partida para un jugador. 
	 * @return ResponseBean Se obtienen los datos del juego si se ha podido crear. En caso contrario game estaría vacío
	 */
	private ResponseBean addNewPlayer() {
		ResponseBean responseBean = null;
		List<NameValuePair> listParams = getRandomUserParams();
		Request request = new Request();
		String response = request.httpPostSimple(URL_PATH + ":" + URL_PORT + "/elegirJuego", listParams.toArray(new NameValuePair[listParams.size()]));
		if (response != null && !response.isEmpty()) {	
			responseBean = getGson().fromJson(response, ResponseBean.class);
			Game game = responseBean.getGame();
			if (game != null) {
				System.out.println("[INFO] Nueva partida de " + game.getGameType().toString() + ". ## Usuario: " + game.getUser().getUserId() + " ## Proveedor: " + game.getUser().getProviderId());	
			}
		}
		
		return responseBean;
	}
	
	
	/**
	 * Se llama al servicio para realizar una apuesta para el juego
	 * @param game Juego
	 * @param betValue Valor de la apuesta
	 * @return ResponseBean Resultado de la llamada al servicio
	 */
	private ResponseBean bet(Game game, Double betValue) {
		ResponseBean responseBean = null;
		List<NameValuePair> listParams = new ArrayList<NameValuePair>();
		listParams.add(new BasicNameValuePair("gameId", game.getGameConfiguration().getGameId()));
		listParams.add(new BasicNameValuePair("bet", String.valueOf(betValue)));
		
		Request request = new Request();
		String response = request.httpPostSimple(URL_PATH + ":" + URL_PORT + "/apostar", listParams.toArray(new NameValuePair[listParams.size()]));
		if (response != null && !response.isEmpty()) {
			responseBean = getGson().fromJson(response, ResponseBean.class);
			
			//En el caso de que se genere una transacción se muestra el detalle de ésta
			if (responseBean.getTransaction() != null) {
				System.out.println("[INFO] Transacción: " + responseBean.getTransaction().getTransactionId() + " ### Usuario: "
						+ responseBean.getTransaction().getGame().getUser().getUserId() + " ## Incremento: "
						+ responseBean.getTransaction().getIncrement() + " ## Balance: "
						+ responseBean.getTransaction().getGame().getUser().getBalance() + " ## Juego: "
						+ responseBean.getTransaction().getGame().getGameType());
				
				//Este mensaje solo se mostraría cuando el usuario se haya quedado sin saldo con la transacción realizada
				if (responseBean.getError() != null)  {
					System.out.println("[INFO] Mensaje: " + responseBean.getError() + " ## GameId: " + game.getGameConfiguration().getGameId() + " ## Usuario: " + game.getUser().getUserId());
				}
			}
			//Si no se ha generado la transacción por cualquier motivo se muestra el detalle
			else {
				System.out.println("[ERROR] Mensaje: " + responseBean.getError() + " ## GameId: " + game.getGameConfiguration().getGameId() + " ## Usuario: " + game.getUser().getUserId());
			}
		}
		
		return responseBean;
	}
		
	
	/* ------------------------- */
	/* ---  HELPER METHODS  --- */
	/* ------------------------- */
	
	/** Genera datos aleatorios de prueba de un Usuario obtenido de X Proveedor
	 * @return List<NameValuePair> listado de parametro-valor
	 */
	private List<NameValuePair> getRandomUserParams() {
		
		String provider = "";
		String balance = "";
		String gameTypeString = "";
		
		if (Math.random() > 0.5) {
			provider = "BWIN";
		}
		else {
			provider = "Poker Star";
		}
		
		int randGame = ((int) (Math.random() * 4) + 1) % 5;
		switch (randGame) {
		case 0:
			gameTypeString = Game.GAME_TYPE.BLACKJACK.toString();
			break;
		case 1:
			gameTypeString = Game.GAME_TYPE.POKER.toString();
			break;
		case 2:
			gameTypeString = Game.GAME_TYPE.RULETA.toString();
			break;
		case 3:
			gameTypeString = Game.GAME_TYPE.SLOT.toString();
			break;
		case 4:
			gameTypeString = Game.GAME_TYPE.VIDEOBINGO.toString();
			break;
		default:
			break;
		}
		
		balance = String.valueOf(Math.round((Math.random() * 1000) + 50));
		
		
		List<NameValuePair> listParams = new ArrayList<NameValuePair>();
		listParams.add(new BasicNameValuePair("userId", UUID.randomUUID().toString()));
		listParams.add(new BasicNameValuePair("providerId", provider));
		listParams.add(new BasicNameValuePair("balance", balance));
		listParams.add(new BasicNameValuePair("gameType", gameTypeString));
		
		return listParams;
	}
	
	/** Obtiene un valor de apuesta aleatorio para un juego. Hay una pequeña probabilidad de que el valor no se encuentre dentro de los
	 * valores válidos para así provocar un error.
	 * @param game Datos del juego
	 * @return Double Valor aleatorio de apuesta
	 */
	private Double getRandomBet(Game game) {
		GameConfiguration config = game.getGameConfiguration();
		Double randomBet = -1d;
		
		//Se añade un 15% de probabilidad de que la apuesta esté fuera del rango, para que así se produzca error de vez en cuando
		if (Math.random() <= 0.15d) {
			randomBet = config.getMinBet() - 1;
		} else {
			if (game.getUser().getBalance() >= config.getMinBet()) { 
				randomBet = Math.random() * (config.getMaxBet() - config.getMinBet()) + config.getMinBet();
				
				if (randomBet > game.getUser().getBalance()) {
					randomBet = game.getUser().getBalance();
				}
			}
			else {
				randomBet = game.getUser().getBalance();
			}
		}
		return BigDecimal.valueOf(randomBet).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
	}
	
	/** 
	 * Éste método obtiene un objeto Gson que permite deserializar la clase abstracta Game
	 * @return Gson Objeto Gson
	 */
	private Gson getGson() {
		GsonBuilder gsonBilder = new GsonBuilder();
		gsonBilder.registerTypeAdapter(Game.class, new GameAdapter());
		Gson gson = gsonBilder.create();
		return gson;
	}

}
