package com.zitro.casino.bean;

import com.zitro.casino.domain.Game;
import com.zitro.casino.domain.Transaction;

public class ResponseBean {
	private boolean endGame = false;
	private String error = null;
	private Transaction transaction = null;
	private Game game = null;
	
	public boolean isEndGame() {
		return endGame;
	}
	public void setEndGame(boolean endGame) {
		this.endGame = endGame;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Transaction getTransaction() {
		return transaction;
	}
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
}
