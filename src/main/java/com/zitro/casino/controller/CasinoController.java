package com.zitro.casino.controller;

import java.math.BigDecimal;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zitro.casino.bean.ResponseBean;
import com.zitro.casino.domain.ApplicationData;
import com.zitro.casino.domain.Game;
import com.zitro.casino.domain.GameConfiguration;
import com.zitro.casino.domain.Transaction;
import com.zitro.casino.domain.User;
import com.zitro.casino.domain.games.Blackjack;
import com.zitro.casino.domain.games.Poker;
import com.zitro.casino.domain.games.Ruleta;
import com.zitro.casino.domain.games.Slot;
import com.zitro.casino.domain.games.Videobingo;

@RestController
public class CasinoController {
		
	@Resource(name = "applicationData")
	ApplicationData applicationData;
	
	@RequestMapping(value ="/apostar", method = RequestMethod.POST)
	public ResponseBean transacion(HttpServletRequest request) {
		ResponseBean responseBean = new ResponseBean();
		String gameId = request.getParameter("gameId");
		String bet = request.getParameter("bet");
		
		Double betValue =  Double.valueOf(bet);
		
		Game game = applicationData.getCurrentGames().get(gameId);
		
		//No hay ninguna sesión activa - Aunque en las pruebas no pasaría nunca por aquí
		if (game == null) {
			responseBean.setEndGame(true);
			responseBean.setError("No hay ninguna sesión activa.");
		}
		else {
			//Ha acabado el tiempo de juego
			if (new Date().getTime() > game.getUser().getMaxPlayingTime()) {
				responseBean.setEndGame(true);
				applicationData.getCurrentGames().remove(game.getGameConfiguration().getGameId());
				responseBean.setError("Ha acabado el tiempo de juego.");
			}
			//La apuesta no está dentro de los valores aceptados
			else  if (betValue < game.getGameConfiguration().getMinBet() || betValue > game.getGameConfiguration().getMaxBet()) {
				responseBean.setError("La apuesta introducida de " + betValue + " debe estar entre los valores " + game.getGameConfiguration().getMinBet() + " y " + game.getGameConfiguration().getMaxBet());
			}
			//Realizar jugada
			else if (game.getUser().getBalance() >= betValue) {
				Transaction transaction = playGame(game, betValue);
				responseBean.setTransaction(transaction);
			}
			//Se ha realizado una apuesta que sobrepasa el Balance
			else {
				responseBean.setEndGame(true);
				applicationData.getCurrentGames().remove(game.getGameConfiguration().getGameId());
				responseBean.setError("Se ha realizado una apuesta que sobrepasa el Balance.");
			}
			
			//Si el balance después de la jugada no es suficiente para continuar jugando, o desde un inicio ya es inferior, finalizar
			if (game.getUser().getBalance() < game.getGameConfiguration().getMinBet()) {
				responseBean.setEndGame(true);
				responseBean.setError("No dispones de saldo suficiente para seguir jugando.");
				applicationData.getCurrentGames().remove(game.getGameConfiguration().getGameId());
			}
		}
		
		return responseBean;
	} 
	
	//Está hecho solo para que el juego elegido sea Blackjack
	@RequestMapping(value ="/elegirJuego", method = RequestMethod.POST)
	public ResponseBean game(HttpServletRequest request) {
		ResponseBean responseBean = new ResponseBean();
		
		Game game = null;
		
		//Comprobamos que no se supere la cantidad máxima de jugadores
		if (applicationData.getCurrentGames().size() < ApplicationData.MAX_PLAYER_GAMES) {
			//Un jugador se conecta
			User user = new User();
			user.setUserId(request.getParameter("userId"));
			user.setBalance(Double.valueOf(request.getParameter("balance"))); //Obtenemos el balance obtenido del proveedor
			user.setProviderId(request.getParameter("providerId")); //Obtenemos el id del proveedor
			//Entre 20 y 60 segundos de tiempo máximo de juego para pruebas
			user.setMaxPlayingTime(new Date().getTime() + (long) Math.round(Math.random() * (40 * 1000)) + 20 * 1000); 
			
			//Se crea el juego elegido y se queda a la espera de que el jugador realice la apuesta
			String gameTypeString = request.getParameter("gameType");
			switch (Game.GAME_TYPE.valueOf(gameTypeString)) {
			case BLACKJACK:
				game = new Blackjack();
				break;
			case POKER:
				game = new Poker();
				break;
			case RULETA:
				game = new Ruleta();
				break;
			case SLOT:
				game = new Slot();
				break;
			case VIDEOBINGO:
				game = new Videobingo();
				break;

			default:
				game = null;
				break;
			}			
			
			if (game != null) {
				game.setGameConfiguration(GameConfiguration.randomConfiguration());
				game.setUser(user);
				applicationData.getCurrentGames().put(game.getGameConfiguration().getGameId(), game);
				responseBean.setGame(game);
			}
			
		}
		else {
			responseBean.setError("Actualmente no se pueden unir más jugadores.");
		}
		return responseBean;
	}
	

	//HELPER METHODS
	private Transaction playGame(Game game, Double bet) {
		GameConfiguration config = game.getGameConfiguration();
		int probability = config.getProbabilityAward();
		BigDecimal increment = BigDecimal.ZERO;
		if ((Math.random() * 100) <= probability) {
			BigDecimal betDecimal = BigDecimal.valueOf(bet);
			BigDecimal result = betDecimal.multiply(BigDecimal.valueOf(config.getAward()));
			increment = result.setScale(2, BigDecimal.ROUND_DOWN);
		}
		else {
			increment = increment.subtract(BigDecimal.valueOf(bet));
		}
		
		game.getUser().setBalance(increment.add(BigDecimal.valueOf(game.getUser().getBalance())).doubleValue());
		
		Transaction transaction = new Transaction();
		transaction.setGame(game);
		transaction.setIncrement(increment.doubleValue());
		
		return transaction;
	}
	
}
