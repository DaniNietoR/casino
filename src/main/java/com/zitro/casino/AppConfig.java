package com.zitro.casino;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.zitro.casino.domain.ApplicationData;

@Configuration
public class AppConfig {
	
	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ApplicationData applicationData() {
        return new ApplicationData();
    }
	
}
