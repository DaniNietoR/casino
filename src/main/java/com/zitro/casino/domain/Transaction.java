package com.zitro.casino.domain;

import java.util.UUID;

public class Transaction {
	private String transactionId;
	private Game game;
	private Double increment;
	
	public Transaction() {
		this.transactionId = UUID.randomUUID().toString();
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public Double getIncrement() {
		return increment;
	}
	public void setIncrement(Double increment) {
		this.increment = increment;
	}
}
