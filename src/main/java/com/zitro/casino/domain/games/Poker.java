package com.zitro.casino.domain.games;

import com.zitro.casino.domain.Game;
import com.zitro.casino.domain.GameConfiguration;

public class Poker extends Game {
	
	public Poker(GameConfiguration config) {
		super(GAME_TYPE.POKER, config);
	}
	
	public Poker() {
		super(GAME_TYPE.POKER);
	}

}
