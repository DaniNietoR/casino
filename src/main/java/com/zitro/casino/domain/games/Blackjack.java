package com.zitro.casino.domain.games;

import com.zitro.casino.domain.Game;
import com.zitro.casino.domain.GameConfiguration;

public class Blackjack extends Game {
	
	public Blackjack(GameConfiguration config) {
		super(GAME_TYPE.BLACKJACK, config);
	}
	
	public Blackjack() {
		super(GAME_TYPE.BLACKJACK);
	}

}
