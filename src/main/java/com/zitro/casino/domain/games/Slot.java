package com.zitro.casino.domain.games;

import com.zitro.casino.domain.Game;
import com.zitro.casino.domain.GameConfiguration;

public class Slot extends Game {
	
	public Slot(GameConfiguration config) {
		super(GAME_TYPE.SLOT, config);
	}
	
	public Slot() {
		super(GAME_TYPE.SLOT);
	}

}
