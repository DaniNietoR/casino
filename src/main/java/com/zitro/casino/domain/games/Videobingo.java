package com.zitro.casino.domain.games;

import com.zitro.casino.domain.Game;
import com.zitro.casino.domain.GameConfiguration;

public class Videobingo extends Game {
	
	public Videobingo(GameConfiguration config) {
		super(GAME_TYPE.VIDEOBINGO, config);
	}
	
	public Videobingo() {
		super(GAME_TYPE.VIDEOBINGO);
	}

}
