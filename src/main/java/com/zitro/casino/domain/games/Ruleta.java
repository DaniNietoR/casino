package com.zitro.casino.domain.games;

import com.zitro.casino.domain.Game;
import com.zitro.casino.domain.GameConfiguration;

public class Ruleta extends Game {
	
	public Ruleta(GameConfiguration config) {
		super(GAME_TYPE.RULETA, config);
	}
	
	public Ruleta() {
		super(GAME_TYPE.RULETA);
	}

}
