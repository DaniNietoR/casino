package com.zitro.casino.domain.json;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.zitro.casino.domain.Game;

/**
 * Clase utilizada para deserializar la clase abstracta Game
 *
 */
public class GameAdapter implements JsonSerializer<Game>, JsonDeserializer<Game> {

	@Override
	public Game deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		JsonObject jsonObject = json.getAsJsonObject();
		String type = jsonObject.get("gameType").getAsString();
		type = type.toLowerCase();
		type = type.substring(0,1).toUpperCase() + type.substring(1);

		try {
			return context.deserialize(json, Class.forName("com.zitro.casino.domain.games." + type));
		} catch (ClassNotFoundException cnfe) {
			throw new JsonParseException("Unknown element type: " + type, cnfe);
		}
	}

	@Override
	public JsonElement serialize(Game src, Type typeOfSrc, JsonSerializationContext context) {
		return context.serialize(src, src.getClass());
	}
	
}
