package com.zitro.casino.domain;

public class User {
	private String userId;
	private Double balance;
	private Long maxPlayingTime; //Timestamp - Tiempo máximo hasta la que se podría jugar
	private String providerId;
	
	public User() {
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Long getMaxPlayingTime() {
		return maxPlayingTime;
	}
	public void setMaxPlayingTime(Long maxPlayingTime) {
		this.maxPlayingTime = maxPlayingTime;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
}
