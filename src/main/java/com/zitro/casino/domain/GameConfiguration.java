package com.zitro.casino.domain;

import java.util.UUID;

public class GameConfiguration {

	private String gameId;
	private Double award;
	private Double minBet;
	private Double maxBet;
	private int probabilityAward;
	
	public GameConfiguration() {
		this.gameId = UUID.randomUUID().toString();
	}
	
	public String getGameId() {
		return gameId;
	}
	public Double getAward() {
		return award;
	}
	/**
	 * @param award Multiplicador para la recompensa de lo apostado
	 */
	public void setAward(Double award) {
		this.award = award;
	}
	public Double getMinBet() {
		return minBet;
	}
	public void setMinBet(Double minBet) {
		this.minBet = minBet;
	}
	public Double getMaxBet() {
		return maxBet;
	}
	public void setMaxBet(Double maxBet) {
		this.maxBet = maxBet;
	}
	public int getProbabilityAward() {
		return probabilityAward;
	}
	public void setProbabilityAward(int probabilityAward) {
		this.probabilityAward = probabilityAward;
	}
	
	public static GameConfiguration randomConfiguration() {
		GameConfiguration config = new GameConfiguration();
		
		config.setAward((double)Math.round(Math.random() * 2) + 1); // El premio será entre 2 y 3 veces lo apostado
		config.setMaxBet((double)Math.round(Math.random() * 450) + 50); //Máxima apuesta entre 50 y 500 €
		config.setMinBet((double)Math.round(Math.random() * 5) + 5); //Minima apuesta entre 5 y 10 €
		config.setProbabilityAward((int) Math.round(Math.random() * 35) + 15); //Probabilidad entre 15 y 50
		
		return config;
	}
}
