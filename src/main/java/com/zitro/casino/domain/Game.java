package com.zitro.casino.domain;

public abstract class Game {

	public static enum GAME_TYPE {VIDEOBINGO,SLOT,BLACKJACK,POKER,RULETA};
	
	protected GAME_TYPE gameType;	
	protected GameConfiguration gameConfiguration;
	protected User user;
	
	public GAME_TYPE getGameType() {
		return gameType;
	}

	protected void setGameType(GAME_TYPE gameType) {
		this.gameType = gameType;
	}

	public GameConfiguration getGameConfiguration() {
		return gameConfiguration;
	}

	public void setGameConfiguration(GameConfiguration gameConfiguration) {
		this.gameConfiguration = gameConfiguration;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Game(GAME_TYPE gameType, GameConfiguration config) {
		this.gameType = gameType;
		this.gameConfiguration = config;
	}
	
	public Game(GAME_TYPE gameType) {
		this.gameType = gameType;
	}

}
