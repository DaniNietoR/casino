package com.zitro.casino.domain;

import java.util.HashMap;

/**
 * Clase que guarda la información de las partidas actuales y de la configuración de la aplicación
 *
 */
public class ApplicationData {
	
	/**
	 * Cantidad máxima de jugadores activos al mismo tiempo
	 */
	public static int MAX_PLAYER_GAMES = 10;
	
	private HashMap<String, Game> currentGames = new HashMap<String, Game>();
	
	public HashMap<String, Game> getCurrentGames() {
		return currentGames;
	}

	public void setCurrentGames(HashMap<String, Game> currentGames) {
		this.currentGames = currentGames;
	}

}
